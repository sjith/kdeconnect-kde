# translation of kdeconnect-kcm.pot to esperanto
# Copyright (C) 2023 Free Software Foundation, Inc.
# This file is distributed under the same license as the kdeconnect-kde package.
# Oliver Kellogg <olivermkellogg@gmail.com, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: kdeconnect-kde\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-20 00:42+0000\n"
"PO-Revision-Date: 2023-12-27 09:20+0100\n"
"Last-Translator: Oliver Kellogg <olivermkellogg@gmail.com>\n"
"Language-Team: Esperanto <kde-i18n-eo@kde.org>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: translate-po (https://github.com/zcribe/translate-po)\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Oliver Kellogg"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "olivermkellogg@gmail.com"

#: kcm.cpp:183
#, kde-format
msgid "Key: %1"
msgstr "Ŝlosilo: %1"

#: kcm.cpp:206
#, kde-format
msgid "Available plugins"
msgstr "Disponeblaj kromprogramoj"

#: kcm.cpp:253
#, kde-format
msgid "Error trying to pair: %1"
msgstr "Eraro provante parigi: %1"

#: kcm.cpp:269
#, kde-format
msgid "(paired)"
msgstr "(parigita)"

#: kcm.cpp:272
#, kde-format
msgid "(not paired)"
msgstr "(ne parigita)"

#: kcm.cpp:275
#, kde-format
msgid "(incoming pair request)"
msgstr "(alvenanta parpeto)"

#: kcm.cpp:278
#, kde-format
msgid "(pairing requested)"
msgstr "(pariĝo petita)"

#. i18n: ectx: property (text), widget (QLabel, rename_label)
#: kcm.ui:59
#, kde-format
msgid "KDE Connect"
msgstr "KDE Connect"

#. i18n: ectx: property (text), widget (QToolButton, renameShow_button)
#: kcm.ui:82
#, kde-format
msgid "Edit"
msgstr "Redakti"

#. i18n: ectx: property (text), widget (QToolButton, renameDone_button)
#: kcm.ui:104
#, kde-format
msgid "Save"
msgstr "Konservi"

#. i18n: ectx: property (text), widget (QPushButton, refresh_button)
#: kcm.ui:120
#, kde-format
msgid "Refresh"
msgstr "Refreŝigi"

#. i18n: ectx: property (text), widget (QLabel, name_label)
#: kcm.ui:193
#, kde-format
msgid "Device"
msgstr "Aparato"

#. i18n: ectx: property (text), widget (QLabel, status_label)
#: kcm.ui:209
#, kde-format
msgid "(status)"
msgstr "(statuso)"

#. i18n: ectx: property (text), widget (KSqueezedTextLabel, verificationKey)
#: kcm.ui:232
#, kde-format
msgid "🔑 abababab"
msgstr "🔑 abababab"

#. i18n: ectx: property (text), widget (QPushButton, cancel_button)
#: kcm.ui:263
#, kde-format
msgid "Cancel"
msgstr "Nuligi"

#. i18n: ectx: property (text), widget (QPushButton, accept_button)
#: kcm.ui:289
#, kde-format
msgid "Accept"
msgstr "Akcepti"

#. i18n: ectx: property (text), widget (QPushButton, reject_button)
#: kcm.ui:296
#, kde-format
msgid "Reject"
msgstr "Malakcepti"

#. i18n: ectx: property (text), widget (QPushButton, pair_button)
#: kcm.ui:309
#, kde-format
msgid "Request pair"
msgstr "Alpeti parigon"

#. i18n: ectx: property (text), widget (QPushButton, unpair_button)
#: kcm.ui:322
#, kde-format
msgid "Unpair"
msgstr "Malparigi"

#. i18n: ectx: property (text), widget (QPushButton, ping_button)
#: kcm.ui:335
#, kde-format
msgid "Send ping"
msgstr "Sendi ping"

#. i18n: ectx: property (text), widget (QLabel, noDeviceLinks)
#: kcm.ui:373
#, kde-format
msgid ""
"<html><head/><body><p>No device selected.<br><br>If you own an Android "
"device, make sure to install the <a href=\"https://play.google.com/store/"
"apps/details?id=org.kde.kdeconnect_tp\"><span style=\" text-decoration: "
"underline;\">KDE Connect Android app</span></a> (also available <a href="
"\"https://f-droid.org/repository/browse/?fdid=org.kde.kdeconnect_tp\"><span "
"style=\" text-decoration: underline;\">from F-Droid</span></a>) and it "
"should appear in the list.<br><br>If you are having problems, visit the <a "
"href=\"https://userbase.kde.org/KDEConnect\"><span style=\" text-decoration: "
"underline;\">KDE Connect Community wiki</span></a> for help.</p></body></"
"html>"
msgstr ""
"<html><head/><body><p>Neniu aparato elektita.<br><br>Se vi posedas Android-"
"aparaton, nepre instalu la <a href=\"https://play.google.com/store/apps/"
"details?id=org.kde.kdeconnect_tp\"><span style=\" text-decoration: underline;"
"\">KDE Connect Android-aplikon</span></a> (ankaŭ havebla <a href =\"https://"
"f-droid.org/repository/browse/?fdid=org.kde.kdeconnect_tp\"><span style=\" "
"text-decoration: underline;\">de F-Droid</span></a>) kaj ĝi devus aperi en "
"la listo.<br><br>Se vi havas problemojn, vizitu la <a href=\"https://"
"userbase.kde.org/KDEConnect\"><span style=\" text-decoration: underline;"
"\">KDE Connect Komunum-vikion</span></a> por helpo.</p></body></html>"
